/* appearance */ static const int sloppyfocus        = 1;  /* focus follows mouse */
static const unsigned int borderpx  = 1;  /* border pixel of windows */
static const float rootcolor[]      = {0.3, 0.3, 0.3, 1.0};
static const float bordercolor[]    = {0, 0, 0, 0};
static const float focuscolor[]     = {0, 0.0, 0.0, 0};

static const int allow_constrain = 1;

/* numlock and capslock */
static const int numlock = 1;
static const int capslock = 0;


/* tagging */
static const char *tags[] = { "Main", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* app_id     title       tags mask     isfloating   monitor */
	/* examples:
	{ "Gimp",     NULL,       0,            1,           -1 },
	{ "firefox",  NULL,       1 << 8,       0,           -1 },
	*/
//	{ "firefox", NULL, 2, 0, 2},
};

/* layout(s) */
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* monitors
 * The order in which monitors are defined determines their position.
 * Non-configured monitors are always added to the left. */
static const MonitorRule monrules[] = {
	/* name       mfact nmaster scale layout       rotate/reflect x y resx resy rate*/
	/* example of a HiDPI laptop monitor:
	{ "eDP-1",    0.5,  1,      2,    &layouts[0], WL_OUTPUT_TRANSFORM_NORMAL, 0, 0},
	*/
	/* defaults */
	{ "DP-2",       0.55, 1,      1,    &layouts[0], WL_OUTPUT_TRANSFORM_NORMAL, 0, 0, 1920, 1080, 144.001007},
	{ "DP-2sdf",       0.55, 1,      1,    &layouts[0], WL_OUTPUT_TRANSFORM_NORMAL, 0, 0, 1920, 1080, 144.001007},
	{ "HDMI-A-2",       0.55, 1,      1,  &layouts[2], WL_OUTPUT_TRANSFORM_NORMAL, 1920, 0, 1920, 1080, 74.544},
	{ NULL,       0.55, 1,      1,    &layouts[0], WL_OUTPUT_TRANSFORM_NORMAL, 0, 0, 0, 0, 0},
};

/* keyboard */
static const struct xkb_rule_names xkb_rules = {
	/* can specify fields: rules, model, layout, variant, options */
	/* example:
	.options = "ctrl:nocaps",
	*/
		.layout = "us",
		.variant = "intlde",
		.options = "caps:escape",

};

static const int repeat_rate = 40;
static const int repeat_delay = 300;

static const float pointer_accel = 0.0;

static const int pointer_profile = LIBINPUT_CONFIG_ACCEL_PROFILE_FLAT;

/* Trackpad */
static const int tap_to_click = 1;
static const int natural_scrolling = 0;

#define MODKEY WLR_MODIFIER_LOGO
#define TAGKEYS(KEY,SKEY,TAG) \
	{ MODKEY,                    KEY,            view,            {.ui = 1 << TAG} }, \
	{ MODKEY|WLR_MODIFIER_CTRL,  KEY,            toggleview,      {.ui = 1 << TAG} }, \
	{ MODKEY|WLR_MODIFIER_SHIFT, SKEY,           tag,             {.ui = 1 << TAG} }, \
	{ MODKEY|WLR_MODIFIER_CTRL|WLR_MODIFIER_SHIFT,SKEY,toggletag, {.ui = 1 << TAG} }

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static const char *termcmd[]  = { "foot", NULL };
static const char *dmenucmd[]  = { "bemenu-run", NULL };
static const char *powermn[] = {"sh", "-c", "~/.local/bin/power.sh"};
static const char *ranger[] = {"foot", "ranger", NULL};
static const char *mutt[] = {"foot", "neomutt", NULL};
static const char *musicy[] = {"foot", "ncmpcpp", NULL};
static const char *newsboat[] = {"foot", "newsboat", NULL};
static const char *emoji[] = {"sh", "-c", "~/.local/bin/emoji.sh", NULL};


static const char *mute[] = {"pactl", "set-source-mute", "@DEFAULT_SOURCE@", "toggle", NULL};

static const char *screenshot[] = {"sh", "-c", "grim ~/desk/bilder/screenshots/scrn-$(date +%d-%m-%H-%M-%S).png", NULL};
static const char *screenclip[]	= {"sh", "-c", "slurp | grim -g - ~/desk/bilder/screenshots/scrn-$(date +%d-%m-%H-%M-%S).png", NULL};
static const char *monshot[] = { "sh", "-c", "grim -o $(echo -e \"DP-2\\nHDMI-A-2\" | bemenu -l 2) ~/desk/bilder/screenshots/$(date +%d-%m-%H-%M-%S).png", NULL };
static const char *mpcplay[] = {"mpc", "toggle", NULL}; static const char *mpcnext[] = {"mpc", "next", NULL}; static const char *mpcprev[] = {"mpc", "prev", NULL};
static const char *mpcvolup[] = {"mpc", "volume", "+5", NULL};
static const char *mpcvoldn[] = {"mpc", "volume", "-5", NULL};

static const char *volup[] = {"pactl", "set-sink-volume", "@DEFAULT_SINK@", "+5%", NULL};
static const char *voldn[] = {"pactl", "set-sink-volume", "@DEFAULT_SINK@", "-5%", NULL};
static const char *pplay[] = {"playerctl", "play-pause", NULL};
static const char *pnext[] = {"playerctl", "next", NULL};
static const char *pprev[] = {"playerctl", "prev", NULL};
static const char *audioswitch[] = {"sh", "-c", "pactl set-default-sink $(pactl list short sinks | grep -v $(pactl get-default-sink) | cut -d \"\t\" -f 2)", NULL};
static const char *pmute[] = {"pactl", "set-sink-mute", "@DEFAULT_SINK@", "toggle", NULL};

static const char *usby[] = {"sh", "-c", "~/.local/bin/desktops/usby", NULL};
static const char *password[] = { "sh", "-c", "~/.local/bin/password", NULL};

static const Key keys[] = {
	/* Note that Shift changes certain key codes: c -> C, 2 -> at, etc. */
	/* modifier                  key                 function        argument */
	{	WLR_MODIFIER_LOGO,						XKB_KEY_Return,					spawn,				{.v = termcmd} },
	{	WLR_MODIFIER_LOGO|WLR_MODIFIER_ALT,		XKB_KEY_d,						spawn,				{.v = usby}    },
	{	WLR_MODIFIER_LOGO|WLR_MODIFIER_SHIFT,	XKB_KEY_D,						spawn,				{.v = emoji}    },
	{	WLR_MODIFIER_LOGO,              		XKB_KEY_p,						spawn,				{.v = password}    },
	{	WLR_MODIFIER_LOGO,						XKB_KEY_b,						spawn,				{.v = mutt} },
	{	WLR_MODIFIER_LOGO,						XKB_KEY_j,						focusstack,			{.i = +1} },
	{	WLR_MODIFIER_LOGO,						XKB_KEY_d,						spawn,				{.v = dmenucmd} },
	{	WLR_MODIFIER_LOGO,						XKB_KEY_Escape,					spawn,				{.v = powermn} },
	{	WLR_MODIFIER_LOGO,						XKB_KEY_k,						focusstack,			{.i = -1} },
	{	WLR_MODIFIER_LOGO,						XKB_KEY_equal,					incnmaster,			{.i = +1} },
	{	WLR_MODIFIER_LOGO,						XKB_KEY_minus,					incnmaster,			{.i = -1} },
	{	WLR_MODIFIER_LOGO,						XKB_KEY_h,						setmfact,			{.f = -0.05} },
	
	{	WLR_MODIFIER_LOGO,						XKB_KEY_m,						spawn,				{.v = musicy} },
	{	WLR_MODIFIER_LOGO,						XKB_KEY_n,						spawn,				{.v = newsboat} },

	{	WLR_MODIFIER_LOGO,						XKB_KEY_l,						setmfact,			{.f = +0.05} },
	{	WLR_MODIFIER_LOGO|WLR_MODIFIER_SHIFT,	XKB_KEY_Return,					zoom,				{0} },
	{	WLR_MODIFIER_LOGO,						XKB_KEY_Tab,					view,				{0} },
	{	WLR_MODIFIER_LOGO|WLR_MODIFIER_SHIFT,	XKB_KEY_Q,						killclient,			{0} },
	{	WLR_MODIFIER_LOGO|WLR_MODIFIER_SHIFT,	XKB_KEY_T,						setlayout,			{.v = &layouts[0]} },
	{	WLR_MODIFIER_LOGO|WLR_MODIFIER_SHIFT,	XKB_KEY_F,						setlayout,			{.v = &layouts[1]} },
	{	WLR_MODIFIER_LOGO|WLR_MODIFIER_SHIFT,	XKB_KEY_H,						setlayout,			{.v = &layouts[2]} },
	{	WLR_MODIFIER_LOGO|WLR_MODIFIER_SHIFT,	XKB_KEY_space,					setlayout,			{0} },
	{	WLR_MODIFIER_LOGO,						XKB_KEY_space,					togglefloating,		{0} },
	{	WLR_MODIFIER_LOGO,						XKB_KEY_f,						togglefullscreen,	{0} },
	{	WLR_MODIFIER_LOGO,						XKB_KEY_0,						view,				{.ui = ~0} },
	{	WLR_MODIFIER_LOGO|WLR_MODIFIER_SHIFT,	XKB_KEY_parenright,				tag,				{.ui = ~0} }, {	WLR_MODIFIER_LOGO,						XKB_KEY_comma,					focusmon,			{.i = WLR_DIRECTION_LEFT} },
	{	WLR_MODIFIER_LOGO,						XKB_KEY_period,					focusmon,			{.i = WLR_DIRECTION_RIGHT} },
	{	WLR_MODIFIER_LOGO|WLR_MODIFIER_SHIFT,	XKB_KEY_less,					tagmon,				{.i = WLR_DIRECTION_LEFT} },
	{	WLR_MODIFIER_LOGO|WLR_MODIFIER_SHIFT,	XKB_KEY_greater,				tagmon,				{.i = WLR_DIRECTION_RIGHT} },
		TAGKEYS(								XKB_KEY_1,	XKB_KEY_exclam,							0),
		TAGKEYS(								XKB_KEY_2,	XKB_KEY_at,								1),
		TAGKEYS(								XKB_KEY_3,	XKB_KEY_numbersign,						2),
		TAGKEYS(								XKB_KEY_4,	XKB_KEY_dollar,							3),
		TAGKEYS(								XKB_KEY_5,	XKB_KEY_percent,						4),
		TAGKEYS(								XKB_KEY_6,	XKB_KEY_caret,							5),
		TAGKEYS(								XKB_KEY_7,	XKB_KEY_ampersand,						6),
		TAGKEYS(								XKB_KEY_8,	XKB_KEY_asterisk,						7),
		TAGKEYS(								XKB_KEY_9,	XKB_KEY_parenleft,						8),
	{	WLR_MODIFIER_LOGO,						XKB_KEY_e,						spawn,				{.v = ranger} },
	{	WLR_MODIFIER_CTRL|WLR_MODIFIER_ALT,		XKB_KEY_Terminate_Server,		quit,				{0}	},
	{	MODKEY|WLR_MODIFIER_SHIFT,				XKB_KEY_E,						quit,				{0}	},
	{	0,										XKB_KEY_XF86AudioLowerVolume,	spawn,				{.v = voldn } },
	{	0,										XKB_KEY_XF86AudioRaiseVolume,	spawn,				{.v = volup } },
	{	0,										XKB_KEY_XF86AudioMute,			spawn,				{.v = pmute } },
	{	0,										XKB_KEY_XF86AudioPlay,			spawn,				{.v = pplay } },
	{	0,										XKB_KEY_XF86AudioNext,			spawn,				{.v = pnext } },
	{	0,										XKB_KEY_XF86AudioPrev,			spawn,				{.v = pprev } },
	{	0,										XKB_KEY_XF86AudioStop,			spawn,				{.v = audioswitch } },

	{	WLR_MODIFIER_LOGO|WLR_MODIFIER_SHIFT,	XKB_KEY_Left,					spawn,				{.v = mpcplay } },
	{	WLR_MODIFIER_LOGO,						XKB_KEY_Right,					spawn,				{.v = mpcnext } },
	{	WLR_MODIFIER_LOGO,						XKB_KEY_Left,					spawn,				{.v = mpcprev } },
	{	WLR_MODIFIER_LOGO,						XKB_KEY_Up,						spawn,				{.v = mpcvolup } },
	{	WLR_MODIFIER_LOGO,						XKB_KEY_Down,					spawn,				{.v = mpcvoldn } },

	{	WLR_MODIFIER_LOGO,						XKB_KEY_s,						spawn,				{.v = screenshot} },
	{	WLR_MODIFIER_LOGO|WLR_MODIFIER_ALT,		XKB_KEY_s,						spawn,				{.v = screenclip} },
	{	WLR_MODIFIER_LOGO|WLR_MODIFIER_SHIFT,	XKB_KEY_S,						spawn,				{.v = monshot}	  },
	{	WLR_MODIFIER_LOGO,						XKB_KEY_u,						spawn,				{.v = mute } },

#define CHVT(n) { WLR_MODIFIER_CTRL|WLR_MODIFIER_ALT,XKB_KEY_XF86Switch_VT_##n, chvt, {.ui = (n)} }
	CHVT(1), CHVT(2), CHVT(3), CHVT(4), CHVT(5), CHVT(6),
	CHVT(7), CHVT(8), CHVT(9), CHVT(10), CHVT(11), CHVT(12),
};

static const Button buttons[] = {
	{ MODKEY, BTN_LEFT,   moveresize,     {.ui = CurMove} },
	{ MODKEY, BTN_MIDDLE, togglefloating, {0} },
	{ MODKEY, BTN_RIGHT,  moveresize,     {.ui = CurResize} },
};
