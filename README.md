dotfiles
========

Just my configs for various programs

dwl
---
Uses a lot of patches, so probably don't use it?

foot
----
It doesn't differenciate between dark and light colors,
because that way, ncurses looks better. 
It's transparent and uses a beam instead of a block.
![foot](screenshot/fetch.png)

ncmpcpp
-------
Made with the help of Brodie's video on ncmpcpp, I think
especially the red bar at the bottom looks cool
![ncmpcpp](screenshot/ncmpcpp.png)

newsboat
--------
Just stolen from Luke Smith

nvim
----
Made to look like Doom Emacs using the [doom-one theme](https://github.com/romgrk/doom-one.vim)
After closing, the cursor is reset to beam and no deleting beyond the line.
Config is split into 2 parts because 1 I migrated from vim and 2 easier config of root account
![nvim](screenshot/vim_c.png)
![nvim](screenshot/vim_bash.png)

waybar
------
Fairly clean looking, but requires [font-awesome](https://github.com/FortAwesome/Font-Awesome) for the symbols.
The config is split in two for each monitor, because of the way that dwlbar is used.
There's also a second style.css
![waybar](screenshot/waybar.png)
