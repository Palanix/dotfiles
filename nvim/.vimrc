let mapleader =" "
set termguicolors

call plug#begin('~/.config/nvim/plugged')
Plug 'romgrk/doom-one.vim'
call plug#end()

runtime! archlinux.vim
set number relativenumber
syntax on
set noerrorbells
set tabstop=4 softtabstop=4
set smartindent
set noswapfile
set nobackup
set undodir=~/.config/nvim/undodir
set undofile
set incsearch
set nocompatible

colorscheme doom-one

set colorcolumn=160

set backspace=start
