set runtimepath+=/home/palanix/.config/nvim/
set packpath+=/home/palanix/.config/nvim/
source /home/palanix/.config/nvim/.vimrc

augroup Shape
		autocmd!
		autocmd VimLeave * set guicursor=a:ver90
augroup END
